import gc
import hashlib
import json
import math
import os
import zipfile

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from PIL import Image, ImageSequence, ImageFile
from numba import jit
from tqdm import tqdm

import constants

ImageFile.LOAD_TRUNCATED_IMAGES = True


@jit(nopython=True)
def calc_one_axis(clow, chigh, pad, cmax):
    clow = max(0, clow - pad)
    chigh = min(cmax, chigh + pad)
    return clow, chigh, chigh - clow


def calc_bbox(obj, img_w, img_h, zoom=0.0, try_square=True):
    xmin = int(obj[0])
    ymin = int(obj[1])
    xmax = int(obj[2])
    ymax = int(obj[3])

    # occasionally i get bboxes which exceed img size
    xmin, xmax, obj_w = calc_one_axis(xmin, xmax, 0, img_w)
    ymin, ymax, obj_h = calc_one_axis(ymin, ymax, 0, img_h)

    if zoom != 0.0:
        pad_w = obj_w * zoom / 2
        pad_h = obj_h * zoom / 2
        xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad_w, img_w)
        ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad_h, img_h)

    if try_square:
        # try pad both sides equaly
        if obj_w > obj_h:
            pad = (obj_w - obj_h) / 2
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = (obj_h - obj_w) / 2
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

        # if it's still not square, try pad where possible
        if obj_w > obj_h:
            pad = obj_w - obj_h
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = obj_h - obj_w
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

    return int(xmin) + 2, int(ymin) + 2, int(xmax) - 4, int(ymax) - 4


@jit(nopython=True)
def bb2wh(bbox):
    width = bbox[2] - bbox[0]
    height = bbox[3] - bbox[1]
    return width, height


def convert_png(root, path):
    img = Image.open(root + path)
    # path = path.split('/')[-1]

    if path.split('.')[-1] == 'gif':
        for epoch, frame in enumerate(ImageSequence.Iterator(img)):
            frame.save(
                root + str(epoch) + "_" + path.replace('gif', 'png'))
        os.remove(root + path)
    else:
        img.convert('RGB').save(root + path)


def PIL_read(path):
    img = Image.open(path, mode='r')
    try:
        img = img.convert('RGB')
    except:
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(img)
    return img


def get_annotator(input_file):
    low_file = open("./datasets/low_percent_file.txt", "w+")
    ratio_intruder = open("./datasets/ratio_intruder.txt", "w+")
    mapper = {}

    if os.path.isfile(constants.json_annotation):
        with open(constants.json_annotation, 'r') as f:
            mapper = json.load(f)
        return mapper

    curr_flag = False
    with open(input_file, 'r') as f:
        lines = f.readlines()
        for line in tqdm(lines):
            if line != '':
                if line.find('Image Path: ') != -1:
                    path_ = line[line.find('Image Path: ') + 12: line.find(': Predicted')]
                    path_ = constants.rawdata_path + path_.split('/')[-1]
                    curr_flag = False
                    if not os.path.isfile(path_):
                        continue
                    curr_flag = True
                    img = PIL_read(path_)
                    path = path_
                    width, height = img.size
                    mapper[path] = []
                else:
                    if line.find('motorbike') != -1:
                        if line.find('(') == -1:
                            continue
                        percent = line[line.find(": ") + 2: line.find("%")]
                        if int(percent) < 80:
                            continue
                        s = line[line.find('(') + 1:line.find(')')].replace(' ', '')
                        i1, i2, i3, i4 = s.find('left_x:'), s.find('top_y:'), s.find('width:'), s.find('height:')
                        x, y, w, h = int(s[i1 + 7:i2]), int(s[i2 + 6:i3]), int(s[i3 + 6:i4]), int(s[i4 + 7:])
                        if int(percent) < 88:
                            low_file.write(
                                path + "_" + percent + "_" + str(round(h / w)) + "_" + str(x)
                                + "_" + str(y) + "_" + str(x + w) + "_" + str(y + h) + '\n')
                            continue
                        if h / w > 3.5:
                            ratio_intruder.write(
                                path + "_" + percent + "_" + str(round(h / w)) + "_" + str(x)
                                + "_" + str(y) + "_" + str(x + w) + "_" + str(y + h) + '\n')

                        if curr_flag:
                            mapper[path].append([max(x, 0), max(y, 0), min(x + w, width), min(y + h, height)])

    low_file.close()
    mapper = {key: value for key, value in mapper.items() if len(value) > 0}
    print("After filtering the empty mapper: ", len(mapper))
    with open(constants.json_annotation, 'w') as f:
        json.dump(mapper, f)
    return mapper


def data_preparation(annotation_file):
    light_zoom, medium_zoom = 0.08, 0.12
    label_mapper = get_annotator(annotation_file)
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            prev_bbox, tfm_imgs = None, None
            bbox = calc_bbox(anno_bbox, img_w=width, img_h=height, zoom=light_zoom)
            ratio = (bbox[3] - bbox[1]) / (bbox[2] - bbox[0])

            if ratio < 0.2 or ratio > 4.0:
                continue

            img_cropped = image.crop(bbox)  # [h,w,c]
            # Interpolation method
            if bbox[2] - bbox[0] > constants.image_width:
                interpolation = Image.ANTIALIAS  # shrink
            else:
                interpolation = Image.CUBIC  # expansion

            img_cropped = img_cropped.resize((constants.image_width, constants.image_height), interpolation)
            img_cropped.save(constants.processed_path + str(i) + "_" + path.split('/')[-1])
    del label_mapper


def data_preparation1(annotation_file):
    label_mapper = get_annotator(annotation_file)
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            xmin = int(anno_bbox[0])
            ymin = int(anno_bbox[1])
            xmax = int(anno_bbox[2])
            ymax = int(anno_bbox[3])

            # xmin = max(0, xmin - 4)  # 4 : margin
            # xmax = min(width, xmax + 4)
            # ymin = max(0, ymin - 4)
            # ymax = min(height, ymax + 4)

            w = np.max((xmax - xmin, ymax - ymin))
            w = min(w, width, height)  # available w

            # ratio = (ymax - ymin) / (xmax - xmin)
            # if ratio < 0.2 or ratio > 4.0:
            #   return None

            if w > xmax - xmin:
                xmin = min(max(0, xmin - int((w - (xmax - xmin)) / 2)), width - w)
                xmax = xmin + w
            if w > ymax - ymin:
                ymin = min(max(0, ymin - int((w - (ymax - ymin)) / 2)), height - w)
                ymax = ymin + w

            # img_cropped = image.crop((xmin, ymin, w, w))  # [h,w,c]
            # img_cropped =
            img_cropped = image.crop((xmin, ymin, xmax, ymax))  # [h,w,c]
            # Interpolation method
            if xmax - xmin > constants.image_width:
                interpolation = Image.ANTIALIAS  # shrink
            else:
                interpolation = Image.CUBIC  # expansion

            img_cropped = img_cropped.resize((constants.image_width, constants.image_height), interpolation)
            img_cropped.save(constants.processed_path + str(i) + "_" + path.split('/')[-1])
    del label_mapper


def remove_duplicate(data_root, print_=False, rm=True):
    imgs_list = os.listdir(data_root)
    print("Before removing duplicate images: " + str(len(imgs_list)))
    duplicates = []
    hash_keys = dict()
    for index, filename in enumerate(imgs_list):
        path = data_root + filename
        if os.path.isfile(path):
            with open(path, 'rb') as f:
                filehash = hashlib.md5(f.read()).hexdigest()
            if filehash not in hash_keys:
                hash_keys[filehash] = index
            else:
                duplicates.append((index, hash_keys[filehash]))
    if print_:
        print(duplicates)
    if rm:
        for index in duplicates:
            os.remove(data_root + imgs_list[index[0]])
    print("After removing duplicate images: " + str(len(os.listdir(data_root))))


def data_preparation2(annotation_file, processed_folder, image_size=None, x_overlap_threshold=0,
                      y_overlap_threshold=0):
    label_mapper = get_annotator(annotation_file)
    processed_folder = processed_folder + "/"
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            xmin = int(anno_bbox[0])
            ymin = int(anno_bbox[1])
            xmax = int(anno_bbox[2])
            ymax = int(anno_bbox[3])

            remaining_bboxs = annotation[:i] + annotation[i + 1:]
            if xmax - xmin > ymax - ymin:
                offset = xmax - xmin - (ymax - ymin)
                nearest_bottom = 0
                nearest_under = height
                for bbox in remaining_bboxs:
                    r_xmin = int(bbox[0])
                    r_ymin = int(bbox[1])
                    r_xmax = int(bbox[2])
                    r_ymax = int(bbox[3])
                    overlap_seg = min(xmax, r_xmax) - max(xmin, r_xmin)
                    if overlap_seg < 0 or overlap_seg - x_overlap_threshold < 0:
                        if r_ymax < ymin:
                            nearest_bottom = max(nearest_bottom, r_ymax)
                        elif r_ymin > ymax:
                            nearest_under = min(nearest_under, r_ymin)
                bottom_offset = min(int(offset / 2), ymin - nearest_bottom)
                under_offset = min(int(offset / 2), nearest_under - ymax)
                if ymin - bottom_offset < 0:
                    under_offset += bottom_offset - ymin
                    bottom_offset = ymin
                if ymax + under_offset > height:
                    bottom_offset += ymax + under_offset - height
                    under_offset = height - ymax

                ymin -= bottom_offset
                ymax += under_offset

            else:
                offset = ymax - ymin - (xmax - xmin)
                if (ymax - ymin) / (xmax - xmin) > 3:
                    continue
                nearest_left = 0
                nearest_right = width

                for bbox in remaining_bboxs:
                    r_xmin = int(bbox[0])
                    r_ymin = int(bbox[1])
                    r_xmax = int(bbox[2])
                    r_ymax = int(bbox[3])
                    overlap_seg = min(ymax, r_ymax) - max(ymin, r_ymin)
                    if overlap_seg < 0 or overlap_seg - y_overlap_threshold < 0:
                        if r_xmax < xmin:
                            nearest_left = max(nearest_left, r_xmax)
                        elif r_xmin > xmax:
                            nearest_right = min(nearest_right, r_xmin)

                left_offset = min(int(offset / 2), xmin - nearest_left)
                right_offset = min(int(offset / 2), nearest_right - xmax)
                if xmin - left_offset < 0:
                    right_offset += left_offset - xmin
                    left_offset = xmin
                if xmax + right_offset > width:
                    left_offset += xmax + right_offset - width
                    right_offset = width - xmax

                xmin -= left_offset
                xmax += right_offset
            if xmax - xmin > (ymax - ymin):
                white_image = Image.new('RGB',
                                        (xmax - xmin, xmax - xmin),  # A4 at 72dpi
                                        (255, 255, 255))  # White
                paste_postition = (0, int((xmax - xmin - ymax + ymin) / 2))
                white_image.paste(image.crop((xmin, ymin, xmax, ymax)), paste_postition)
                white_image.save(processed_folder + str(i) + "_" + path.split('/')[-1])
                continue
            img_cropped = image.crop((xmin, ymin, xmax, ymax))
            # Interpolation method
            # if image_size:
            #     if xmax - xmin > image_size:
            #         interpolation = Image.ANTIALIAS  # shrink
            #     else:
            #         interpolation = Image.CUBIC  # expansion
            #
            #     img_cropped = img_cropped.resize((image_size, image_size), interpolation)
            # try:
            img_cropped.save(processed_folder + str(i) + "_" + path.split('/')[-1])


def load_np_images(sample_size=25000, resize=False, image_width=constants.image_width,
                   image_height=constants.image_height,
                   image_channels=constants.image_channels):
    curIdx = 0
    images_np = np.zeros((sample_size, image_width, image_height, image_channels))
    for path in tqdm(os.listdir(constants.processed_path)):
        img = PIL_read(os.path.join(constants.processed_path, path))
        if resize:
            if max(img.width, img.height) > image_width:
                interpolation = Image.ANTIALIAS  # shrink
            else:
                interpolation = Image.CUBIC  # expansion

            img = img.resize((image_width, image_width), interpolation)
        img = np.array(img)
        # Convert RGB to BGR
        img = img[:, :, ::-1]
        images_np[curIdx, :, :, :] = np.asarray(img)
        curIdx += 1

    return images_np[:min(curIdx, sample_size), :, :, :]


def makedirs(path):
    os.makedirs(path, exist_ok=True)


def plot_features(features, examples=25):
    if not math.sqrt(examples).is_integer():
        print('Please select a valid number of examples.')
        return

    imgs = []
    for i in range(examples):
        imgs.append(features[i, :, :, :])

    fig, axes = plt.subplots(round(math.sqrt(examples)), round(math.sqrt(examples)), figsize=(15, 15),
                             subplot_kw={'xticks': [], 'yticks': []},
                             gridspec_kw=dict(hspace=0.3, wspace=0.01))

    for i, ax in enumerate(axes.flat):
        ax.imshow(imgs[i])
    if constants.plot_show:
        plt.show()
    plt.close()
    gc.collect()


def plot_losses(G_losses, D_losses, all_gl, all_dl, epoch, dir):
    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss - EPOCH {}".format(epoch))
    plt.plot(G_losses, label="G")
    plt.plot(D_losses, label="D")
    plt.xlabel("Iterations")
    plt.ylabel("Loss")
    plt.legend()
    ymax = plt.ylim()[1]
    plt.savefig(dir + "/loss_epoch_{}.png".format(epoch))
    if constants.plot_show:
        plt.show()

    plt.figure(figsize=(10, 5))
    plt.plot(np.arange(len(all_gl)), all_gl, label='G')
    plt.plot(np.arange(len(all_dl)), all_dl, label='D')
    plt.legend()
    # plt.ylim((0,np.min([1.1*np.max(all_gl),2*ymax])))
    plt.title('All Time Loss')
    plt.savefig(dir + "/all_loss_epoch.png")
    if constants.plot_show:
        plt.show()
    plt.close()
    gc.collect()


def generate_and_save_images(model, epoch, test_input, dir, rows=8, cols=8):
    # Notice `training` is set to False.
    # This is so all layers run in inference mode (batchnorm).
    predictions = model(test_input, training=False)
    fig = plt.figure(figsize=(14, 14))
    for i in range(predictions.shape[0]):
        plt.subplot(rows, cols, i + 1)
        plt.imshow((predictions[i, :, :, :] * 127.5 + 127.5) / 255.)
        plt.axis('off')

    plt.subplots_adjust(wspace=0, hspace=0)
    plt.savefig(dir + '/image_at_epoch_{:04d}.png'.format(epoch))
    if constants.plot_show:
        plt.show()
    plt.close()
    gc.collect()


def generate_test_image(model, noise_dim=constants.noise_dim):
    test_input = tf.random.normal([1, noise_dim])
    # Notice `training` is set to False.
    # This is so all layers run in inference mode (batchnorm).
    predictions = model(test_input, training=False)
    fig = plt.figure(figsize=(5, 5))
    plt.imshow((predictions[0, :, :, :] * 127.5 + 127.5) / 255.)
    plt.axis('off')
    if constants.plot_show:
        plt.show()
    plt.close()
    gc.collect()


def zip_images(generator, filename='images.zip'):
    # SAVE TO ZIP FILE NAMED IMAGES.ZIP
    z = zipfile.PyZipFile(filename, mode='w')
    for k in range(constants.num_examples_to_generate):
        generated_image = generator(tf.random.normal([1, constants.noise_dim]), training=False)
        f = str(k) + '.png'
        img = np.array(generated_image)
        img = (img[0, :, :, :] + 1.) / 2.
        img = Image.fromarray((255 * img).astype('uint8').reshape(
            (constants.image_height, constants.image_width, constants.image_channels)))
        img.save(f, 'PNG')
        z.write(f)
        os.remove(f)
        # if k % 1000==0: print(k)
    z.close()
    gc.collect()
    print('Saved final images for submission.')


def get_score(evaluator, generator, image_sample_size, dir, rows=8, cols=8):
    imgs = np.ones((image_sample_size, constants.image_height, constants.image_width, 3))
    for k in range(image_sample_size):
        generated_image = generator(tf.random.normal([1, constants.noise_dim]), training=False)
        img = np.array(generated_image)
        img = (img[0, :, :, :] + 1.) / 2.
        img = (255 * img).astype('uint8').reshape(
            (constants.image_height, constants.image_width, constants.image_channels))
        imgs[k, :, :, :] = img

    fig = plt.figure(figsize=(14, 14))
    for i in range(min(imgs.shape[0], constants.num_examples_to_generate)):
        plt.subplot(rows, cols, i + 1)
        plt.imshow(imgs[i, :, :, :] / 255.)
        plt.axis('off')

    plt.subplots_adjust(wspace=0, hspace=0)
    mifid = evaluator.compute_mifid(imgs)
    plt.savefig(dir + "/image_mifid_{:.4}.png".format(mifid))
    gc.collect()
    return mifid


def save_images(generator, directory):
    imgs = np.ones((constants.image_sample_size, constants.image_height, constants.image_width, 3))
    for k in range(constants.image_sample_size):
        generated_image = generator(tf.random.normal([1, constants.noise_dim]), training=False)
        f = str(k) + '.png'
        f = os.path.join(directory, f)
        img = np.array(generated_image)
        img = (img[0, :, :, :] + 1.) / 2.
        img = Image.fromarray((255 * img).astype('uint8').reshape(
            (constants.image_height, constants.image_width, constants.image_channels)))
        imgs[k, :, :, :] = img
        img.save(f, 'PNG')
        # if k % 1000==0: print(k)
    gc.collect()
    return imgs
