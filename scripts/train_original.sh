#!/usr/bin/env bash
python3 train.py
--npz_file ./datasets/npz_data.npz
--sample_size 22125
--batch_size 64
--experiment_name original
--lr_initial_g 0.0002 --lr_initial_d 0.0002
--loss_version gan
--display_results 15
--calculate_mifid 30
--decay_step 50 --lr_decay_steps 12
--epochs 280
--spectral_normalization
--leaky_relu_slope 0.2
--scale_factor 16 # 4 ** downsize_factor
--gaussian_noise
--downsize_factor 2