#!/usr/bin/env bash
python3 train.py
--sample_size 22125
--batch_size 64
--experiment_name original
--lr_initial_g 0.0002 --lr_initial_d 0.0002
--loss_version gan
--display_results 40
--calculate_mifid 100
--decay_step 50 --lr_decay_steps 12
--replay_step 50
--epochs 280
--spectral_normalization
--replay_step
--leaky_relu_slope 0.2
--scale_factor 16 # 4 ** downsize_factor
--downsize_factor 2
--dropout_rate 0.5