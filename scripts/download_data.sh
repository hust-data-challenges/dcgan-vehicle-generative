#!/usr/bin/env bash
mkdir datasets
mkdir datasets/raw

wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/training_dataset.zip
#wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/evaluation_script.zip
wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/motor_gen_128.zip

unzip training_dataset.zip -d datasets/raw
#unzip evaluation_script.zip -d datasets
unzip motor_gen_128.zip -d datasets

rm training_dataset.zip
#rm evaluation_script.zip
rm motor_gen_128.zip