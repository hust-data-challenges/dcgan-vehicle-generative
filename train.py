import numpy as np
import tensorflow as tf
from tqdm import tqdm

tf.enable_eager_execution()
import dcgan
import transformer
from constants import *
from dataset import VehicleLoader
from evaluation.client.mifid_demo import MIFID
from losses import generator_loss, discriminator_loss
from prepare_config import prepare_parser
from utils import plot_losses, generate_and_save_images, get_score, makedirs, save_images


def train_step(images, generator, discriminator,
               generator_optimizer, discriminator_optimizer,
               batch_size, noise_dim, loss_type='gan', add_gaussian=False):
    noise = tf.random_normal([batch_size, noise_dim])
    gaussian_noise = 0
    if add_gaussian:
        gaussian_noise = tf.random.normal(shape=(batch_size, 128, 128, 3), mean=0.0, stddev=0.1, dtype=tf.float32)

    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        generated_images = generator(noise, training=True)

        if add_gaussian:
            images += gaussian_noise
            real_output = discriminator(images, training=True)
        else:
            real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)

        gen_loss = generator_loss(real_output, fake_output, loss_type, apply_label_smoothing=True)
        disc_loss = discriminator_loss(real_output, fake_output, loss_type,
                                       apply_label_smoothing=True, label_noise=True)

    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))

    return gen_loss, disc_loss


def run(config):
    experiment_name = (config['experiment_name'] if config['experiment_name'] + "/"
                       else 'generative_vehicle_images/')
    print('Experiment name is %s' % experiment_name)

    transforms = [transformer.flip]
    loader = VehicleLoader(config['npz_file'], config['sample_size'], transforms, config['batch_size'])

    experimental_output = experiments_root + experiment_name
    output_images = submissions_root + experiment_name
    samples_output = samples_root + experiment_name
    temporal_outputs = temporals_root + experiment_name
    checkpoint_dir = checkpoints_root + experiment_name

    makedirs(experimental_output)
    makedirs(output_images)
    makedirs(samples_output)
    makedirs(temporal_outputs)
    makedirs(checkpoint_dir)

    generator = dcgan.Generator(config)
    discriminator = dcgan.Discriminator(config)

    print(generator.summary())
    print(discriminator.summary())

    generator_optimizer = tf.train.AdamOptimizer(learning_rate=config['lr_initial_g'], beta1=0.5)
    discriminator_optimizer = tf.train.AdamOptimizer(learning_rate=config['lr_initial_d'], beta1=0.5)

    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                     discriminator_optimizer=discriminator_optimizer,
                                     generator=generator,
                                     discriminator=discriminator)

    if config['resume'] and len(os.listdir(checkpoint_dir)) > 0:
        checkpoint.restore(checkpoint_dir)

    # evaluator = MIFID(evaluation_model, public_feature_path)
    # training
    all_gl = np.array([])
    all_dl = np.array([])

    exp_replay = []
    mifid = -1.0
    best_mifid = 700.0
    for epoch in tqdm(range(config['epochs'])):
        G_loss = []
        D_loss = []

        clock.tic()
        new_lr_d = config['lr_initial_d']
        new_lr_g = config['lr_initial_g']
        global_step = 0
        print('Beginning training at epoch %d...' % epoch)
        for image_batch in loader:
            g_loss, d_loss = train_step(image_batch,
                                        generator, discriminator,
                                        generator_optimizer, discriminator_optimizer,
                                        config['batch_size'], noise_dim,
                                        config['loss_version'], config['gaussian_noise'])

            global_step = global_step + 1
            G_loss.append(g_loss.numpy().mean())
            D_loss.append(d_loss.numpy().mean())
            all_gl = np.append(all_gl, np.array([G_loss]))
            all_dl = np.append(all_dl, np.array([D_loss]))

        # generate an extra image for each epoch and store it in memory for experience replay
        if config['replay_step'] != -1:
            generated_image = generator(tf.random.normal([1, noise_dim]), training=False)
            exp_replay.append(generated_image)
            if len(exp_replay) == config['replay_step']:
                print('Executing experience replay..')
                replay_images = np.array([p[0] for p in exp_replay])
                discriminator(replay_images, training=True)
                exp_replay = []

        # display.clear_output(wait=True)
        if (epoch + 1) % config['display_results'] == 0 or epoch == 0:
            plot_losses(G_loss, D_loss, all_gl, all_dl, epoch + 1, experimental_output)
            generate_and_save_images(generator, epoch + 1, seed, dir=samples_output)

        if (epoch + 1) % config['calculate_mifid'] == 0:
            evaluator = MIFID(evaluation_model, public_feature_path)
            mifid = get_score(evaluator, generator, image_sample_size, temporal_outputs)
            if mifid < best_mifid:
                best_mifid = mifid
                files = os.listdir(checkpoint_dir)
                for file in files:
                    os.remove(os.path.join(checkpoint_dir, file))
                checkpoint_prefix = os.path.join(checkpoint_dir, f"fid_{mifid:.4}_ckpt")
                checkpoint.save(file_prefix=checkpoint_prefix)

            del evaluator

            # Cosine learning rate decay
        if (epoch + 1) % config['decay_step'] == 0:
            new_lr_d = tf.train.cosine_decay(new_lr_d, min(global_step, config['lr_decay_steps']),
                                             config['lr_decay_steps'])
            new_lr_g = tf.train.cosine_decay(new_lr_g, min(global_step, config['lr_decay_steps']),
                                             config['lr_decay_steps'])
            generator_optimizer = tf.train.AdamOptimizer(learning_rate=new_lr_d, beta1=0.5)
            discriminator_optimizer = tf.train.AdamOptimizer(learning_rate=new_lr_g, beta1=0.5)

        curr_time_str, elapsed = clock.date_toc()
        log = "[{}][Ep {}/{} - time[{:.2} minute]: \n".format(curr_time_str, epoch + 1,
                                                              config['epochs'],
                                                              clock.toc() / 60) + \
              ''.join(['\t   Gen_loss [mean: {:.4}, std: {:.4}]\n'.format(np.mean(G_loss), np.std(G_loss)),
                       '\t   Disc_loss [mean: {:.4}, std: {:.4}]\n'.format(np.mean(D_loss), np.std(D_loss)),
                       '\t   FID: {:.4}'.format(mifid)])

        print(log)
        del G_loss
        del D_loss

    # Generate after the final epoch
    # display.clear_output(wait=True)
    # final_seed = tf.random.normal([64, noise_dim])
    evaluator = MIFID(evaluation_model, public_feature_path)
    np_imgs = save_images(generator, submissions_root)
    mifid = evaluator.compute_mifid(np_imgs)
    del np_imgs
    if mifid < best_mifid:
        files = os.listdir(checkpoint_dir)
        for file in files:
            os.remove(checkpoint_dir + file)
        checkpoint_prefix = os.path.join(checkpoint_dir, f"fid_{mifid:.4}_ckpt")
        checkpoint.save(file_prefix=checkpoint_prefix)
    print('Final epoch.')


def main():
    # parse command line and run
    parser = prepare_parser()
    config = vars(parser.parse_args())
    print(config)
    run(config)


if __name__ == '__main__':
    main()
