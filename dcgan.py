from tensorflow.keras import Sequential
from tensorflow.keras.layers import Reshape, \
    Flatten, Dropout

from modules import *


# from IPython import display

# libraries for SpectralNorm

def Generator(config):
    model = Sequential()
    model.add(Dense(image_width // config['scale_factor'] * image_height // config['scale_factor'] * 128,
                    input_shape=(noise_dim,), kernel_initializer=weight_initializer))
    # model.add(BatchNormalization(epsilon=BN_EPSILON, momentum=BN_MOMENTUM))
    # model.add(LeakyReLU(alpha=leaky_relu_slope))
    model.add(Reshape((image_height // config['scale_factor'], image_width // config['scale_factor'], 128)))

    model = transposed_conv(model, 512, ksize=5, stride_size=1)
    model.add(Dropout(config['dropout_rate']))
    model = transposed_conv(model, 256, ksize=5, stride_size=2)
    model.add(Dropout(config['dropout_rate']))
    model = transposed_conv(model, 128, ksize=5, stride_size=2)
    model = transposed_conv(model, 64, ksize=5, stride_size=2)
    model = transposed_conv(model, 32, ksize=5, stride_size=2)

    model.add(Dense(3, activation='tanh', kernel_initializer=weight_initializer))

    return model


def Discriminator(config):
    model = Sequential()
    if config['spectral_normalization']:
        model.add(ConvSN2D(64, (5, 5), strides=(1, 1), padding='same', use_bias=False,
                           input_shape=[image_height, image_width, image_channels],
                           kernel_initializer=weight_initializer))
        # model.add(BatchNormalization(epsilon=BN_EPSILON, momentum=BN_MOMENTUM))
        model.add(LeakyReLU(alpha=config['leaky_relu_slope']))
        if config['add_dropout1']:
            model.add(Dropout(dropout_rate))  # add because of what to overfitted on discriminator by mine

        model = convSN(model, 64, ksize=5, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 128, ksize=3, stride_size=1, leaky_relu_slope=config['leaky_relu_slope'])
        model = convSN(model, 128, ksize=5, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 256, ksize=3, stride_size=1, leaky_relu_slope=config['leaky_relu_slope'])
        model = convSN(model, 256, ksize=5, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 512, ksize=3, stride_size=1, leaky_relu_slope=config['leaky_relu_slope'])

        if config['add_dropout2']:
            model.add(Dropout(dropout_rate))  # add because of what to overfitted on discriminator by mine

        model.add(Flatten())
        model.add(DenseSN(1, activation='sigmoid'))
    else:
        model.add(Conv2D(64, (4, 4), strides=(2, 2), padding='same', use_bias=False,
                         input_shape=[image_height, image_width, image_channels],
                         kernel_initializer=weight_initializer))
        # model.add(BatchNormalization(epsilon=BN_EPSILON, momentum=BN_MOMENTUM))
        model.add(LeakyReLU(alpha='leaky_relu_slope'))
        # model.add(Dropout(dropout_rate))

        model = conv(model, 64, ksize=4, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 128, ksize=3, stride_size=1)
        model = conv(model, 128, ksize=4, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 256, ksize=3, stride_size=1)
        model = conv(model, 256, ksize=4, stride_size=2, leaky_relu_slope=config['leaky_relu_slope'])
        # model = convSN(model, 512, ksize=3, stride_size=1)

        model.add(Flatten())
        model.add(Dense(1, activation='sigmoid'))
    return model
