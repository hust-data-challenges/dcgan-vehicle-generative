import os

from timer import Timer

project_path = os.path.abspath(".") + "/"
data_root = project_path + "datasets/"
annotation_file = data_root + "label.txt"
npz_data = data_root + "processed_image.npz"
json_annotation = data_root + "annotation.json"
evaluation_model = project_path + "evaluation/client/motorbike_classification_inception_net_128_v4_e36.pb"
public_feature_path = project_path + "evaluation/client/public_feature.npz"

rawdata_path = data_root + "raw/motobike/"
missingdata_path = data_root + "missing_images/"
processed_path = data_root + "processed_data/"
checkpoints_root = project_path + "checkpoints/"
samples_root = project_path + "samples/"
experiments_root = project_path + "experiments/"
submissions_root = project_path + "submission/"
temporals_root = project_path + "temporal_images/"

num_examples_to_generate = 64
image_sample_size = 10000
image_width = 128
image_height = 128
image_channels = 3
weight_init_std = 0.02
weight_init_mean = 0.0
noise_dim = 128
output_dim = 64
plot_show = True
dropout_rate = 0.5
seed = 1234

missing_paths = [
    'cqtqdo_VRF_750_800_for_new_rider__hd93yl7kong31_1568720006698_20021.gif',
    '80_motorcycle_application1_1568719028982_6503.gif',
    '55_gia_xe_81_2_1568719088201_7338.gif',
    '24_kawasaki_vulcan_s_abs_2016_moto_saigon_3_1568719119960_7778.gif',
    'Motorheiow5b5smallMotor.jpg',
    'd34z0h_My_first_edit_ever_with_my_first_camera_ever_and_my_first_bike_ever_____camera_just_came_in_today__I_feel_like_I_know_absolutely_nothing_about_photography_and_I_can_t_wait_to_get_better__hfy1fbmik4m31_1568720023653_20196.jpg',
    'd04uf0_My_first_bike___It_s_too_much_bike___2002_Kawasaki_Vulcan_1500_mean_streak__Hypercharger__power_commander__Vance__amp__Hines_pipes___1900_off_of_fb_market__I_have_no_idea_what_I_m_doing_rjs551ccltk31_1568720022054_20178.jpg',
    '29_pic_001_1568719608045_14577.gif',
    '69_xedoisong_Yamaha_MT_03_do_1_MEGC_1568719472292_12693.gif',
    '19_r_636858959614681208_4528_kkk_1568719429934_12092.gif',
    '65_51ew43mofl9ef_1568719232558_9326.gif'
]

clock = Timer()
