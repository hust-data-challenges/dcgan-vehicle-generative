import os

import numpy as np
import tensorflow as tf

import constants
from utils import plot_features


class VehicleLoader:
    def __init__(self, npz_data, n_samples, transforms=None, batch_size=128, plot_n_samples=-1):
        if n_samples is None:
            n_samples = len(os.listdir(constants.processed_path))
        with np.load(npz_data, 'rb') as f:
            images = f['data']
        images = images[:min(n_samples, len(images)), :, :, :]
        images = (images - 127.5) / 127.5
        self.transforms = transforms
        self.sample_size = min(n_samples, len(images))
        self.initialization(images, batch_size)
        if plot_n_samples > 0:
            self.plot_sample(images, plot_n_samples)
        print(f"Loaded vehicle {min(n_samples, len(images))} dataset with shape {images.shape} successfully!")
        self.get_range(images)
        del images

    def plot_sample(self, images, n_samples):
        plot_features((images * 127.5 + 127.5) / 255., n_samples)

    def get_range(self, images):
        print("Range value: ", np.max(images[3, :, :, :]), np.min(images[3, :, :, :]))

    def initialization(self, images, batch_size):
        features_tf = tf.cast(images, 'float32')
        features_data = tf.data.Dataset.from_tensor_slices(features_tf).shuffle(self.sample_size)
        if self.transforms:
            for transform in self.transforms:
                features_data = features_data.map(transform)
        self.features_data = features_data.batch(batch_size, drop_remainder=True)
        print(features_data)

    def __iter__(self):
        for data in self.features_data:
            yield data
