import os

import click
import numpy as np

import constants
from constants import clock
from utils import convert_png, data_preparation2, load_np_images, makedirs, remove_duplicate


def prepare(gen_again=False, handle_missing=True):
    if handle_missing and (
            not os.path.isdir(constants.processed_path) or len(os.listdir(constants.processed_path)) == 0):
        for path in constants.missing_paths:
            convert_png(constants.rawdata_path, path)

    remove_duplicate(data_root=constants.rawdata_path)

    clock.tic()
    makedirs(constants.data_root)
    makedirs(constants.processed_path)
    makedirs(constants.samples_root)
    makedirs(constants.experiments_root)
    print("Time of intialization-path stage: ", clock.toc())
    clock.tic()
    if gen_again:
        if os.path.isdir(constants.processed_path):
            for f in os.listdir(constants.processed_path):
                os.remove(constants.processed_path + f)
        data_preparation2(constants.annotation_file, constants.processed_path)
    print("Time to run data-preprocessing stage : ", clock.toc())


def get_npzdataset(gen_again=False, n_sample=0, resize=True):
    clock.tic()
    if n_sample == 0:
        n_sample = len(os.listdir(constants.processed_path))
    if gen_again or not os.path.isfile(constants.npz_data):
        images_np = load_np_images(sample_size=n_sample, resize=resize)
        np.savez_compressed(constants.npz_data, data=images_np)
    else:
        with np.load(constants.npz_data, 'rb') as f:
            images_np = f['data']
    print("Time to get numpy data from file : ", clock.toc())
    return images_np


@click.command()
@click.option("--feature", required=True)
@click.option("--gen_again", required=False, default=False)
@click.option("--n_sample", required=False, default=0)
@click.option("--handle_missing", required=True, default=0)
def run(feature, gen_again, n_sample, handle_missing):
    if feature == 'prepare':
        prepare(gen_again, handle_missing)
    elif feature == 'gen_npz':
        get_npzdataset(gen_again, n_sample)


if __name__ == '__main__':
    run()
