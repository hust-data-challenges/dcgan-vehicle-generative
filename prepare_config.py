from argparse import ArgumentParser, ArgumentTypeError


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'True', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'False', 'f', 'n', '0'):
        return False
    else:
        raise ArgumentTypeError('Boolean value expected.')


def prepare_parser():
    usage = 'Parser for all scripts.'
    parser = ArgumentParser(description=usage)

    ### Dataset/Dataloader stuff ###
    parser.add_argument(
        '--experiment_name', type=str, default="Original")
    parser.add_argument(
        '--npz_file', type=str, default="Original")
    parser.add_argument(
        '--sample_size', type=int, default=0)
    parser.add_argument(
        '--batch_size', type=int, default=32)
    parser.add_argument(
        '--lr_initial_g', type=float, default=0.0002)
    parser.add_argument(
        '--lr_initial_d', type=float, default=0.0002)
    parser.add_argument(
        '--loss_version', type=str, default='gan')
    parser.add_argument(
        '--display_results', type=int, default=25)
    parser.add_argument(
        '--calculate_mifid', type=int, default=50)
    parser.add_argument(
        '--gaussian_noise', action='store_true', default=False)
    parser.add_argument(
        '--resume', action='store_true', default=False)
    parser.add_argument(
        '--smoothing', action='store_true', default=True)
    parser.add_argument(
        '--label_noise', action='store_true', default=True)
    parser.add_argument(
        '--add_dropout1', action='store_true', default=False)
    parser.add_argument(
        '--add_dropout2', action='store_true', default=True)

    ### Model stuff ###
    parser.add_argument(
        '--decay_step', type=int, default=50)
    parser.add_argument(
        '--lr_decay_steps', type=int, default=12)
    parser.add_argument(
        '--replay_step', type=int, default=-1)
    parser.add_argument(
        '--epochs', type=int, default=280)
    parser.add_argument(
        '--spectral_normalization', action='store_true', default=True)

    ### Numerical precision and SV stuff ###
    parser.add_argument(
        '--leaky_relu_slope', type=float, default=0.2)
    parser.add_argument(
        '--scale_factor', type=int, default=4 ** 2)
    parser.add_argument(
        '--downsize_factor', type=int, default=2)
    parser.add_argument(
        '--dropout_rate', type=float, default=0.5)
    return parser


# Arguments for sample.py; not presently used in train.py
def add_sample_parser(parser):
    parser.add_argument(
        '--sample_num', type=int, default=10000,
        help='Number of images to sample'
             '(default: %(default)s)')
    parser.add_argument(
        '--trunc_z', type=float, default=0.0,
        help='Truncate noise vector to this value'
             '(default: %(default)s)')
    return parser
